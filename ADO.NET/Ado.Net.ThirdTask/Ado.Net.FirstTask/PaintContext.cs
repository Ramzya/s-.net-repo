﻿using Ado.Net.FirstTask.Models;
using System;
using System.Data.Entity;
using System.Collections.Generic;

namespace Ado.Net.ThirdTask
{

    public class PaintContext: DbContext
    {
        static PaintContext()
        {
            Database.SetInitializer(new MyDataInitializer());
        }
        public PaintContext()
            : base("DBConnection")
        { }
        public DbSet<Painting> Paints { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }
        public DbSet<Master_alt> Master_Alts { get; set; }
        public DbSet<Exhibition> Exhibitions { get; set; }
        public DbSet<Customer> Customers { get; set; }

    }
    class MyDataInitializer : DropCreateDatabaseAlways<PaintContext>
    {
        protected override void Seed(PaintContext db)
        {
            Master_alt m1 = new Master_alt {MasterName="Samson",DateofBirth=DateTime.Now};
            Master_alt m2 = new Master_alt { MasterName = "Samson", DateofBirth = DateTime.Now };
            Master_alt m3 = new Master_alt { MasterName = "Samson", DateofBirth = DateTime.Now };
            Painting p1 = new Painting { Name = "Storm", DateOfCreation = DateTime.Today, Style = "future", Cost = 12311, MaterId = 1, ExhebitionId = 1 };
            Painting p2 = new Painting { Name = "Storm", DateOfCreation = DateTime.Today, Style = "future", Cost = 12311, MaterId = 1, ExhebitionId = 1 };
            Painting p3 = new Painting { Name = "Storm", DateOfCreation = DateTime.Today, Style = "future", Cost = 12311, MaterId = 1, ExhebitionId = 1 };
            Customer c1 = new Customer { Name = "sam", Country = "Canada", City = "Toronto", OrderNumber = 1, PostCode = 12344 };
            Customer c2 = new Customer { Name = "sam", Country = "Canada", City = "Toronto", OrderNumber = 1, PostCode = 12344 };
            Customer c3 = new Customer { Name = "sam", Country = "Canada", City = "Toronto", OrderNumber = 1, PostCode = 12344 };
            Exhibition e1 = new Exhibition { Name = "someEx", DateOfBegining = DateTime.Now, DateOfEnding = DateTime.MaxValue, PaintingSold = 4, TicketsSold = 23445 };
            Exhibition e2 = new Exhibition { Name = "someEx", DateOfBegining = DateTime.Now, DateOfEnding = DateTime.MaxValue, PaintingSold = 4, TicketsSold = 23445 };
            Exhibition e3 = new Exhibition { Name = "someEx", DateOfBegining = DateTime.Now, DateOfEnding = DateTime.MaxValue, PaintingSold = 4, TicketsSold = 23445 };
            OrderDetails o1 = new OrderDetails { OrderNumber = 1, PaintingId = 1, ExhebitionId = 1 };
            OrderDetails o2 = new OrderDetails { OrderNumber = 2, PaintingId = 1, ExhebitionId = 1 };
            OrderDetails o3 = new OrderDetails { OrderNumber = 3, PaintingId = 1, ExhebitionId = 1 };

            db.Master_Alts.AddRange(new List<Master_alt>() { m1, m2, m3});
            db.Paints.AddRange(new List<Painting> { p1, p2, p3 });
            db.Customers.AddRange(new List<Customer> { c1, c2, c3 });
            db.Exhibitions.AddRange(new List<Exhibition> { e1, e2, e3 });
            db.OrderDetails.AddRange(new List<OrderDetails> { o1, o2, o3 });
            db.SaveChanges();
        }
    }
}

