﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ado.Net.FirstTask.Models
{
    public class OrderDetails
    {   [Key]
        public int OrderNumber { get; set; }
        public int PaintingId { get; set; }
        [ForeignKey("Painting")]
        public int ExhebitionId { get; set; }
        public virtual ICollection<Painting> Painting { get; set; }
        
    }
}
