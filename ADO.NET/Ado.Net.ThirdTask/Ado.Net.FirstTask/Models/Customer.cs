﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ado.Net.FirstTask.Models
{
    public class Customer
    {
        public string Name { get; set; }
        [Key]
        public int OrderNumber { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public int PostCode { get; set; }
        public virtual OrderDetails OrderDetails { get; set; }
       
    }
}
