﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ado.Net.FirstTask.Models
{
    public class Painting
    {   [Key]
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Style { get; set; }
        public int Cost { get; set; }
        public DateTime DateOfCreation { get; set; }
        public int MaterId { get; set; }
        public int ExhebitionId { get; set; }
        public virtual Exhibition Exhibition { get; set; }
        public virtual Master_alt Master_Alt{ get; set; }
        public virtual OrderDetails OrderDetails { get; set; }
       
    }
}
