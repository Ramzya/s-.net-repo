﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ado.Net.FirstTask.Models
{
    public class Exhibition
    {  [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBegining { get; set; }
        public DateTime DateOfEnding { get; set; }
        public int TicketsSold { get; set; }
        public int PaintingSold { get; set; }
        public virtual ICollection<Painting> Painting { get; set; }
    }
}
