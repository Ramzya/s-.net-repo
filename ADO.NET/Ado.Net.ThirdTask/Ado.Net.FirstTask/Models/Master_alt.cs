﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Ado.Net.FirstTask.Models
{
    public class Master_alt
    {   [Key]
        public int Id { get; set; }
        public string MasterName { get; set; }
        public DateTime DateofBirth { get; set; }
        public virtual ICollection<Painting> Painting { get; set; }

       
    }
}
