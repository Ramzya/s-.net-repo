namespace Ado.Net.FirstTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Paintings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Style = c.String(),
                        Cost = c.Int(nullable: false),
                        DateOfCreation = c.DateTime(nullable: false),
                        MaterId = c.Int(nullable: false),
                        ExhebitionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Paintings");
        }
    }
}
