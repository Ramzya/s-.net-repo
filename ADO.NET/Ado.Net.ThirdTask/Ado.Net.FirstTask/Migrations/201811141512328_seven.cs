namespace Ado.Net.FirstTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seven : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        OrderNumber = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        PostCode = c.Int(nullable: false),
                        OrderDetails_OrderNumber = c.Int(),
                    })
                .PrimaryKey(t => t.OrderNumber)
                .ForeignKey("dbo.OrderDetails", t => t.OrderDetails_OrderNumber)
                .Index(t => t.OrderDetails_OrderNumber);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        OrderNumber = c.Int(nullable: false, identity: true),
                        PaintingId = c.Int(nullable: false),
                        ExhebitionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderNumber);
            
            CreateTable(
                "dbo.Exhibitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateOfBegining = c.DateTime(nullable: false),
                        DateOfEnding = c.DateTime(nullable: false),
                        TicketsSold = c.Int(nullable: false),
                        PaintingSold = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Master_alt",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MasterName = c.String(),
                        DateofBirth = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Paintings", "Exhibition_Id", c => c.Int());
            AddColumn("dbo.Paintings", "Master_Alt_Id", c => c.Int());
            AddColumn("dbo.Paintings", "OrderDetails_OrderNumber", c => c.Int());
            CreateIndex("dbo.Paintings", "Exhibition_Id");
            CreateIndex("dbo.Paintings", "Master_Alt_Id");
            CreateIndex("dbo.Paintings", "OrderDetails_OrderNumber");
            AddForeignKey("dbo.Paintings", "Exhibition_Id", "dbo.Exhibitions", "Id");
            AddForeignKey("dbo.Paintings", "Master_Alt_Id", "dbo.Master_alt", "Id");
            AddForeignKey("dbo.Paintings", "OrderDetails_OrderNumber", "dbo.OrderDetails", "OrderNumber");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customers", "OrderDetails_OrderNumber", "dbo.OrderDetails");
            DropForeignKey("dbo.Paintings", "OrderDetails_OrderNumber", "dbo.OrderDetails");
            DropForeignKey("dbo.Paintings", "Master_Alt_Id", "dbo.Master_alt");
            DropForeignKey("dbo.Paintings", "Exhibition_Id", "dbo.Exhibitions");
            DropIndex("dbo.Paintings", new[] { "OrderDetails_OrderNumber" });
            DropIndex("dbo.Paintings", new[] { "Master_Alt_Id" });
            DropIndex("dbo.Paintings", new[] { "Exhibition_Id" });
            DropIndex("dbo.Customers", new[] { "OrderDetails_OrderNumber" });
            DropColumn("dbo.Paintings", "OrderDetails_OrderNumber");
            DropColumn("dbo.Paintings", "Master_Alt_Id");
            DropColumn("dbo.Paintings", "Exhibition_Id");
            DropTable("dbo.Master_alt");
            DropTable("dbo.Exhibitions");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Customers");
        }
    }
}
