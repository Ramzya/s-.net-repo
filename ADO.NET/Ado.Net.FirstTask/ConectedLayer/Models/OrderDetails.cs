﻿using System;

namespace Connected.Models
{
    public class OrderDetails
    {
        public int OrderNumber { get; set; }
        public Guid PaintingId { get; set; }
        public Guid ExhebitionId { get; set; }
    }
}
