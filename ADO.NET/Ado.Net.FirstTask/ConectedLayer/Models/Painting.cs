﻿using System;

namespace Connected.Models
{
    public class Painting
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Style { get; set; }
        public int Cost { get; set; }
        public DateTime DateOfCreation { get; set; }
        public Guid MaterId { get; set; }
        public Guid ExhebitionId { get; set; }
    }
}
