﻿namespace Connected.Interfaces
{
    public interface InterfaceADO<ModelRepo>
        where ModelRepo : class
        
    {
        void Incert(ModelRepo model);
        void Delete(int id);
        void Update(ModelRepo model,int id);
    }
}