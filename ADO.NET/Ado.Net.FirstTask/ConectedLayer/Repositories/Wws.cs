﻿
using System;
using System.Data.SqlClient;

namespace Connected.Repositories
{
    public class WorkWithString
    {
        protected delegate void NQueryDelegate(SqlConnection connection);

        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Paintings;Integrated Security=True";

        protected TReturnType ExecuteWithConnection<TReturnType>(Func<SqlConnection, TReturnType> callback)
        {
            using (SqlConnection cn = new SqlConnection(connectionString))
            {
                try
                {
                    cn.Open();
                    return callback(cn);
                }
                catch (SqlException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
        }

        protected void ExecuteNonQuery(string query)
        {
            ExecuteWithConnection((SqlConnection connection) =>
            {
                SqlCommand command = new SqlCommand(query, connection);
                
            });
        }

        protected void ExecuteWithConnection(NQueryDelegate callback)
        {
            using (SqlConnection cn = new SqlConnection(connectionString))
            {
                try
                {
                    cn.Open();
                    callback(cn);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
        }
    }
}

