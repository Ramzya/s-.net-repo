﻿using System;
using CalcLogic;
using IntCalcLogic;

namespace Manager
{
    public class Facad
    {
        private string first;
        private string second;
        private string type;
        private string oper;

        public Validation valid = new Validation();
        
        public void Input()
        {

            do
            {
                Console.WriteLine("Print first digit:");
                first = Console.ReadLine();
                if (valid.IsNumber(first)) { }
                else Console.WriteLine("Type digits please, not letters");
            }
            while (valid.IsNumber(first) == false);

            do
            {
                Console.WriteLine("Print operation:");
                oper = Console.ReadLine();
                if (valid.IsOper(oper)) { }
                else Console.WriteLine("incorrect opperand");
            }
            while (valid.IsOper(oper) == false);

            do
            {
                Console.WriteLine("Print second digit:");
                second = Console.ReadLine();
                if (valid.IsNumber(second)) { }
                else Console.WriteLine("Type digits please, not letters");
            }
            while (valid.IsNumber(second) == false);

            do
            {
                Console.WriteLine("Print the type:");
                type = Console.ReadLine();
                if (valid.IsType(type))
                { }
                else { Console.WriteLine("wrong type. Its must be Float or Int"); }
            }
            while (valid.IsType(type) == false);

        }




        public void DoCalc()
        {
            Handler_I Handler1 = new Handler_I();
            switch (type)
            {
                case "Float":
                    CalcFunction asmyCalc = new CalcFunction();

                    asmyCalc.OnCalculate += Handler1.Message;
                    switch (oper)
                    {
                        case "-":
                            asmyCalc.minus(first, second);
                            break;
                        case "+":
                            asmyCalc.plus(first, second);
                            break;
                        case "/":
                            asmyCalc.divide(first, second);
                            break;
                        case "*":
                            asmyCalc.multiply(first, second);
                            break;
                        default:
                            Console.WriteLine("wrong operand");
                            break;
                    }
                    break;
                case "Int":
                    
                    IntCalcFunction myCalc = new IntCalcFunction();
                    myCalc.OnCalculate += Handler1.Message;
                    switch (oper)
                    {
                        case "-":
                            myCalc.minus(first, second);
                            break;
                        case "+":
                            myCalc.plus(first, second);
                            break;
                        case "/":
                            myCalc.divide(first, second);
                            break;
                        case "*":
                            myCalc.multiply(first, second);
                            break;
                        default:
                            Console.WriteLine("wrong operand");
                            break;
                    }
                    break;

                default:
                    Console.WriteLine("wrong type");
                    break;
            }



        }
    }
}



