﻿using System;
using ClassLibrary3;


namespace CalcLogic

{


    public class CalcFunction : AbstractCalc
    {
        public delegate void CalcDel(string msg);
        public event CalcDel OnCalculate;

        float result;

        public override void plus(string a, string b)
        {
            result = (float.Parse(a) + float.Parse(b));
            Calculate(result);
        }
        public override void minus(string a, string b)
        {
            result = (float.Parse(a) - float.Parse(b));
            Calculate(result);
        }
        public override void divide(string a, string b)
        {
            result = (float.Parse(a) / float.Parse(b));
            Calculate(result);
        }
        public override void multiply(string a, string b)
        {
            result = (float.Parse(a) * float.Parse(b));
            Calculate(result);
        }
        public void Calculate(float msg)
        {
            OnCalculate($"Calculating was succesful:{msg}");
        }
    }


}
