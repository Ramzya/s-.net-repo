﻿using System;
using ClassLibrary3;

namespace IntCalcLogic

{

    public class IntCalcFunction : AbstractCalc
    {
        int result;
        public delegate void CalcDel(string msg);
        public event CalcDel OnCalculate;
        public override void plus(string a, string b)
        {
            result = (int.Parse(a) + int.Parse(b));
            Calculate(result);
        }
        public override void minus(string a, string b)
        {
            result = (int.Parse(a) - int.Parse(b));
            Calculate(result);
        }
        public override void divide(string a, string b)
        {
            result = (int.Parse(a) /int.Parse(b));
            Calculate(result);
        }
        public override void multiply(string a, string b)
        {
            result = (int.Parse(a) * int.Parse(b));
            Calculate(result);
        }
        public void Calculate(int msg)
        {
            OnCalculate($"Calculating was succesful:{msg}");
        }
    
    }
}