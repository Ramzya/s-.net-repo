﻿using System.Collections.Generic;
using System;

namespace ClassLibrary3
{
    abstract public class AbstractCalc
    {
       
        abstract public void plus(string a, string b);
        abstract public void minus(string a, string b);
        abstract public void divide(string a, string b);
        abstract public void multiply(string a, string b);

    }

}
