﻿using System;
using System.IO;
using System.Xml.Serialization;



namespace PokeAbstract
{
    [Serializable]
    public class Poke<T>
    {
        private T name;
        private T type;
        private int lvl;
        private T skill;
        public Poke()
        {

        }

        public Poke(T n, T t, int l, T s)
        {
            name = n;
            type = t;
            lvl = l;
            skill = s;

        }
        
        public T Name
        {
            get { return name; }
            set { name = value; }

        }

        public T Type
        {
            get { return type; }
            set { type = value; }

        }

        public int Lvl
        {
            get { return lvl; }
            set { lvl = value; }

        }

        public T Skill
        {
            get { return skill; }
            set { skill = value; }

        }
    }
}
