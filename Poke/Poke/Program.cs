﻿using System;



namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("list of valid commands:");
            Console.WriteLine("\nShow \t - \t Display all Pokes in the collection");
            Console.WriteLine("Add \t - \t Add Poke to the collection");
            Console.WriteLine("Remove \t - \t Remove Poke to the collection");
            Console.WriteLine("Edit \t - \t Edit an existing Poke");
            Console.WriteLine("Sort \t - \t Sort the collection");
            Console.WriteLine("Save \t - \t Binary serialization");
            Console.WriteLine("Release  - \t Binary deserialization");
            Console.WriteLine("SaveXml - \t XML serialization");
            Console.WriteLine("ReleaseXml - \t XML deserialization");
            Console.WriteLine("Exit \t - \t Shotdown the Program\n");

            PokeCollection.PokeCollection myColections = new PokeCollection.PokeCollection();
            
            do
            {
                Console.Write("Chose the comand: ");
                string comand = Console.ReadLine();
                switch (comand)
                {
                    case "Show":
                        myColections.Show();
                        break;
                    case "Add":
                        myColections.Add();
                        break;
                    case "Edit":
                        myColections.Edit();
                        break;
                    case "Remove":
                        myColections.Remove();
                        break;
                    case "Sort":
                        myColections.Sort();
                        break;
                    case "Exit":
                        System.Environment.Exit(0);
                        break;
                    case "Save":
                        myColections.PokeSave();
                        break;
                    case "Release":
                        myColections.PokeRelease();
                        break;
                    case "SaveXml":
                        myColections.PokeSaveXml();
                        break;
                    case "ReleaseXml":
                        myColections.PokeReleaseXml();
                        break;
                    default:
                        Console.WriteLine("Wrong comand");
                        break;
                }
            }
            while (true);

        }
    }
}
