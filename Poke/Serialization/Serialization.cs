﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using PokeAbstract;


namespace Serialization
{
    public class Serialize
        {
          BinaryFormatter formatter = new BinaryFormatter();

        public void SavePoke(List<Poke<string>> pokesAray)
        {
            using (FileStream ps = new FileStream("poke.dat", FileMode.OpenOrCreate))
            {

                formatter.Serialize(ps, pokesAray);
                Console.WriteLine("Fine");
            }

        }
        public List<Poke<string>> ReleasePoke()
        {
            using (FileStream ps = new FileStream("poke.dat", FileMode.OpenOrCreate))
            {
                if (ps.Length != 0)
                {
                    Console.WriteLine("ok");
                    return (List<Poke<string>>)formatter.Deserialize(ps);

                }
                else return new List<Poke<string>>();

            }
        }



        XmlSerializer formater = new XmlSerializer(typeof(List<Poke<string>>));
        public void SavePokeXml(List<Poke<string>> pokesAray)
        {
            using (FileStream psx = new FileStream("poke.xml", FileMode.OpenOrCreate))
            {

                formater.Serialize(psx, pokesAray);
                Console.WriteLine("Fine");
            }

        }
        public List<Poke<string>> ReleasePokeXml()
        {
            using (FileStream psx = new FileStream("poke.xml", FileMode.OpenOrCreate))
            {
                if (psx.Length != 0)
                {
                    Console.WriteLine("ok");
                    return (List<Poke<string>>)formater.Deserialize(psx);

                }
                else return new List<Poke<string>>();

            }
        }

    }
}
 
