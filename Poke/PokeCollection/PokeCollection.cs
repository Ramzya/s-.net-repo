﻿using System;
using System.Collections.Generic;
using PokeAbstract;
using Serialization;
using Validation;



namespace PokeCollection
{
    public class PokeCollection
    {

        public List<Poke<string>> pokesArray = new List<Poke<string>>();
        public Validate valid = new Validate();
        Serialize Helper = new Serialize();

        public PokeCollection()
        {
            pokesArray.Add(new Poke<string>("Pikachu", "water", 113, "spark"));
            pokesArray.Add(new Poke<string>("Ash", "earth", 123, "tornado"));
            pokesArray.Add(new Poke<string>("Onix", "earth", 12, "Tremor"));
            pokesArray.Add(new Poke<string>("Charmander", "fire", 41, "fireball"));

        }

        public void PokeSave()
        {
            Helper.SavePoke(pokesArray);

        }
        public void PokeRelease()
        {
            pokesArray = Helper.ReleasePoke();

        }
        public void PokeSaveXml()
        {
            Helper.SavePokeXml(pokesArray);

        }
        public void PokeReleaseXml()
        {
            pokesArray = Helper.ReleasePokeXml();

        }
        public void Show()
        {
            for (int i = 0; i < pokesArray.Count; i++)
            {
                Console.WriteLine("{0}ый poke: {1}, {2}, {3}, {4}", i, pokesArray[i].Name, pokesArray[i].Type, pokesArray[i].Lvl, pokesArray[i].Skill);
            }

        }
        public void Add()
        {
            Console.WriteLine("Fill the next field:");
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Type:");
            string type = Console.ReadLine();
            Console.WriteLine("Level:");
            int lvl = int.Parse(Console.ReadLine());
            Console.WriteLine("Skill:");
            string skill = Console.ReadLine();

            if (valid.IsChar(name) == true && valid.IsChar(name) == true && valid.IsChar(skill) == true)
            {
                pokesArray.Add(new Poke<string>(name, type, lvl, skill));
            }
            else
            {
                Console.WriteLine("ERROR: Some field has an incorrect character, all character must be a letters or string is too long mix size is 10 characters . if it's too hard for u press Alt +F4");

            }

        }
        public void Remove()
        {
            Console.WriteLine("enter the number of the element:");
            int elem = int.Parse(Console.ReadLine());
            pokesArray.RemoveAt(elem);
        }
        string field;
        public void Edit()
        {
            Console.WriteLine("select a Poke:");
            int number = int.Parse(Console.ReadLine());
            Console.WriteLine("select the field:");
            field = Console.ReadLine();
            Console.WriteLine("type the info:");
            string info = Console.ReadLine();

            if (valid.IsChar(info) == true)
            {
                switch (field)
                {
                    case "Name":
                        pokesArray[number].Name = info;

                        break;
                    case "Type":
                        pokesArray[number].Type = info;
                        break;
                    case "Level":
                        if (int.TryParse(info, out int result))
                            pokesArray[number].Lvl = result;
                        else Console.WriteLine("ERROR: all characters must be a digits or string is too long max size is 3 characters.  if it's too hard for u press Alt +F4");
                        break;
                    case "Skill":
                        pokesArray[number].Skill = info;
                        break;
                    default:
                        Console.WriteLine("Wrong field");
                        break;
                }
            }
            else
            {
                Console.WriteLine("ERROR: Some field has an incorrect character, all characters must be a letters.  if it's too hard for u press Alt +F4");

            }


        }
        public void Sort()
        {
            Console.WriteLine("Chose a fiel for sorting:");
            string sortField = Console.ReadLine();
            switch (sortField)
            {
                case "Name":
                    pokesArray.Sort((p1, p2) => string.Compare(p1.Name, p2.Name, true));

                    break;
                case "Type":
                    pokesArray.Sort((p1, p2) => string.Compare(p1.Type, p2.Type, true));
                    break;
                case "Level":
                    pokesArray.Sort((p1, p2) => p1.Lvl.CompareTo(p2.Lvl));
                    break;
                case "Skill":
                    pokesArray.Sort((p1, p2) => string.Compare(p1.Skill, p2.Skill, true));
                    break;
                default:
                    Console.WriteLine("Wrong field");
                    break;
            }

        }













    }
}

