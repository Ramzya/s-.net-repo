﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary3
{
    public interface ICalculator
    {
        void plus(string a, string b);
        void minus(string a, string b);
        void multiply(string a, string b);
        void divide(string a, string b);
    }

}
