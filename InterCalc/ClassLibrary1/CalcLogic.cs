﻿using System;
using ClassLibrary3;


namespace CalcLogic

{
   
    public class CalcFunction : ICalculator
    {
        
        public void plus(string a, string b)
        {
            Console.WriteLine(float.Parse(a) + float.Parse(b)); 
        }
        public void minus(string a, string b)
        {
            Console.WriteLine(float.Parse(a) - float.Parse(b));
        }
        public void divide(string a, string b)
        {

            Console.WriteLine(float.Parse(a) / float.Parse(b));
        }
        public void multiply(string a, string b)
        {
            Console.WriteLine(float.Parse(a) * float.Parse(b));
        }
    }
}
