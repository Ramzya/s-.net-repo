﻿using System;
using ClassLibrary3;

namespace IntCalcLogic

{

    public class IntCalcFunction : ICalculator
    { 
        public void plus(string a, string b)
        {
            Console.WriteLine((int)float.Parse(a) + (int)float.Parse(b));
        }
        public void minus(string a, string b)
        {
            Console.WriteLine((int)float.Parse(a) - (int)float.Parse(b));
        }
        public void divide(string a, string b)
        {

            Console.WriteLine((int)float.Parse(a) / (int)float.Parse(b));
        }
        public void multiply(string a, string b)
        {
            Console.WriteLine((int)float.Parse(a) * (int)float.Parse(b));
        }
    }
}